import React from "react";

let date = String(new Date());

const Hello = (props) => {
	const hello_array = [
		{
			id: 1,
			valor: props.hello,
		},
		{
			id: 2,
			valor: props.hello.split("").join(" "),
		},
		{
			id: 3,
			valor: props.hello.split("").reverse().join(" "),
		},
		{
			id: 4,
			valor: props.hello.split("").reverse().join(""),
		},
		{
			id: 5,
			valor: props.hello.split(" ").reverse().join(""),
		},
		{
			id: 6,
			valor: props.hello.split(" ").reverse().join(" "),
		},
		{
			id: 7,
			valor: props.hello.replace(/(e|o)/gi, ""),
		},
		{
			id: 8,
			valor: props.hello.toLowerCase(),
		},
		{
			id: 9,
			valor: props.hello.toUpperCase(),
		},
		{
			id: 10,
			valor: props.hello.split("").join("-"),
		}
	];

	const listItems = hello_array.map((item) => (
		<div key={item.id}>
			<div className="hello_cards">
				<h2>{item.valor}</h2>
				<h2>{date.substring(0, 24)}</h2>
			</div>
		</div>
	));

	return <div className="hello_flex">{listItems}</div>;
};

export default Hello;
